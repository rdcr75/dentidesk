FROM php:7.4-apache
LABEL maintainer="ruben"

RUN a2enmod rewrite

RUN apt-get update

RUN apt-get update && apt-get install -y \
        libzip-dev \
        libicu-dev \
        libxml2-dev \
        libpq-dev \
        vim \
        curl \
        git \
        supervisor \
        zip \
        unzip \
        && docker-php-ext-install pdo_mysql zip intl xmlrpc soap opcache bcmath \
        && docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd

# nueva linea
# RUN composer require laravel/ui

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -- \
        && apt-get install -y nodejs \
        && apt-get autoremove -y

# nueva linea
# RUN npm install && npm run dev

# nueva linea
# RUN php artisan ui vue --auth

# HACER DESPUES DE CREAR PROYECTO LARAVEL
# RUN chmod -R 777 storage/
# RUN chmod -R 777 bootstrap/cache/

COPY --from=composer /usr/bin/composer /usr/bin/composer

# Suppress the do-not-run-as-root warning from composer
ENV COMPOSER_ALLOW_SUPERUSER 1