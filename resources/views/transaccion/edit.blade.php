@extends('layouts.app')

@section('content')
@guest
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5>Usuario no autorizado</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Editar Transacción</div>
                    <div class="card-body">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger" role="alert">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="/transaccion/{{ $transaccion->id }}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{ method_field('PATCH') }}
                            <input type="hidden" class="form-control" id="id" name="id" value="{{ $transaccion->id }}" readonly >
                            <input type="hidden" class="form-control" id="user_id" name="user_id" value="{{ $transaccion->user_id }}" readonly >
                            <div class="form-inline">
                                <label for="fecha">Fecha ( <?php echo  date('d/m/Y H:i', strtotime($transaccion->fecha)); ?> )</label>
                                <input type="datetime-local" class="form-control" id="fecha" name="fecha" value="{{ date('d/m/Y H:i', strtotime($transaccion->fecha)) }}">
                            </div>
                            <div class="form-group">
                                <label for="descripcion">Descripción</label>
                                <textarea class="form-control" id="descripcion" name="descripcion" rows="5">{{ $transaccion->descripcion }}</textarea>
                            </div>
                            <label for="tipo">Tipo de Transacción</label>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="tipo" value="I" {{ $transaccion->tipo == 'I' ? 'checked' : '' }}>Ingreso
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="tipo" value="E"  {{ $transaccion->tipo == 'E' ? 'checked' : '' }} >Egreso
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="monto">Monto</label>
                                <input type="number" min="1" step="any" class="form-control" id="monto" name="monto" value="{{ $transaccion->monto }}">
                            </div>
                            <input class="btn btn-primary mt-4" type="submit" value="Guardar Transacción">
                        </form>
                        <a class="btn btn-primary float-right" href="/transaccion"><i class="fas fa-arrow-circle-up"></i> Regresar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endguest
@endsection
