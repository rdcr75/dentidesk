@extends('layouts.app')

@section('content')
@guest
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5>Usuario no autorizado</h5>
                </div>
            </div>
        </div>
    </div>
</div>
@else
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h4>Resúmen de Transacciones {{ $anio }}</h4></div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Mes</th>
                                @for ($i = 1; $i < 13; $i++)
                                    <th scope="col">{{ $totales['mes'][$i] }}</td>
                                @endfor
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="col">Ingresos</th>
                                @for ($i = 1; $i < 13; $i++)
                                    <td style="text-align: right">{{ number_format($totales['ingresos'][$i], 2, ',', '.') }}</td>
                                @endfor
                            </tr>
                            <tr>
                                <th scope="col">Egresos</th>
                                @for ($i = 1; $i < 13; $i++)
                                    <td class="egreso">{{ number_format($totales['egresos'][$i], 2, ',', '.') }}</td>
                                @endfor
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">Saldo</th>
                                @for ($i = 1; $i < 13; $i++)
                                    <th scope="col" class="{{ $totales['saldos'][$i] < 0 ? 'egreso' : 'ingreso' }} ">{{ number_format($totales['saldos'][$i], 2, ',', '.') }}</td>
                                @endfor
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-header"><h4>Transacciones Realizadas</h4></div>
                <div class="card-body">
                    <div class="mt-2">
                        <div class="row">
                            <div class="col-3">
                                <a href="/transaccion/create" class="form-control btn btn-success btn-sm">
                                    <i class="fas fa-plus-circle"></i> Nueva Transacción
                                </a>
                            </div>
                            <div class="col-3">
                                &nbsp;
                            </div>
                            <div class="col align-right">
                                <form action="/transaccion" method="get" class="form-inline">
                                    <input type="text" class="form-control" name="fecha" placeholder="Fecha" arial-describedby="search">
                                    &nbsp;<input type="text" class="form-control" name="descripcion" placeholder="Descripcion" arial-describedby="search">
                                    &nbsp;<button type="submit" class="form-control btn btn-secondary btn-sm"><i class="fas fa-search"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <br>
                    <table class="table">
                        <thead>
                            <th scope="col">id</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">tipo</th>
                            <th scope="col">Monto</th>
                            <th scope="col">Acciones</th>
                        </thead>
                        <tbody>
                            @foreach($transacciones as $transaccion)
                            <tr>
                                <th scope="row">{{ $transaccion->id }}</th>
                                <td>{{ $transaccion->fecha }}</td>
                                <td>{{ $transaccion->descripcion }}</td>
                                <td><div class="{{ $transaccion->tipo == 'E' ? 'egreso' : 'ingreso' }}">{{ $transaccion->tipo == 'E' ? '-' : '+' }}</div></td>
                                <td class="{{$transaccion->tipo == 'E' ? 'egreso' : 'ingreso' }}">
                                    {{ number_format($transaccion->monto, 2, ',', '.') }}
                                </td>
                                <td style="text-align: center">
                                    <div class="form-inline">
                                        <form method="post" action="{{ url('/transaccion/'.$transaccion->id.'/edit') }}">
                                            {{ csrf_field() }}
                                            {{ method_field('GET') }}
                                            <button type="submit" class="btn btn-secondary btn-sm">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        </form>
                                        &nbsp;
                                        <form method="post" action="{{ url('/transaccion/'.$transaccion->id) }}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button
                                                type="submit"
                                                class="btn btn-danger btn-sm"
                                                onclick="return confirm('¿Seguro que desea borrarl el registro?')">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $transacciones->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endguest
@endsection
