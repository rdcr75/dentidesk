@extends('layouts.app')

@section('content')

@guest
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5>Usuario no autorizado</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Nueva Transacción</div>
                    <div class="card-body">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger" role="alert">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="/transaccion" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="fecha">Fecha</label>
                                <input type="datetime-local" class="form-control" id="fecha" name="fecha" value="{{old('fecha')}}">
                            </div>
                            <div class="form-group">
                                <label for="descripcion">Descripción</label>
                                <textarea class="form-control" id="descripcion" name="descripcion" rows="5">{{old('descripcion')}}</textarea>
                            </div>
                            <label for="tipo">Tipo de Transacción</label>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="tipo" value="I" {{old('tipo') == 'I' ? 'checked' : '' }}>Ingreso
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="tipo" value="E"  {{old('tipo') == 'E' ? 'checked' : '' }} >Egreso
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="monto">Monto</label>
                                <input type="number" min="1" step="any" class="form-control" id="monto" name="monto" value="{{old('monto')}}">
                            </div>
                            <input class="btn btn-primary mt-4" type="submit" value="Guardar Transacción">
                        </form>
                        <a class="btn btn-primary float-right" href="/transaccion"><i class="fas fa-arrow-circle-up"></i> Regresar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endguest
@endsection
