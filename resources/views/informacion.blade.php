@extends('layouts.app')

@section('page_description')
@section('page_title','Sobre este proyecto')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Información</div>

                <div class="card-body">
                    Sitio Web de prueba para postulación al Cargo de desarrollador PHP con Laravel en la empresa Dentidesk.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection