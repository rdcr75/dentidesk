<?php

namespace App\Http\Controllers;

use App\Transaccion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;

class TransaccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset(Auth::user()->id)){
            $user_id = Auth::user()->id;
        }else{
            $user_id = 1;
        }

        $fecha = $request->get('fecha');
        $descripcion = $request->get('descripcion');

        $transacciones = Transaccion::Fecha($fecha)->Descripcion($descripcion)->where('user_id',$user_id)
                                        ->orderBy('fecha','DESC')
                                        ->paginate(5);
        $i=1;

        $meses = array(
            1 => 'Enero',
            2 => 'Febrero',
            3 => 'Marzo',
            4 => 'Abril',
            5 => 'Mayo',
            6 => 'Junio',
            7 => 'Julio',
            8 => 'Agosto',
            9 => 'Septiembre',
            10 => 'Octubre',
            11 => 'Noviembre',
            12 => 'Diciembre'
        );

        $anio = date('Y');

        for($i==1;$i<=12;$i++){
            $totales['mes'][$i] = $meses[$i];
            $totales['ingresos'][$i] = (float)Transaccion::where('user_id',$user_id)->where('tipo','I')->whereMonth('fecha',$i)->whereYear('fecha',$anio)->sum('monto');
            $totales['egresos'][$i] = (float)Transaccion::where('user_id',$user_id)->where('tipo','E')->whereMonth('fecha',$i)->whereYear('fecha',$anio)->sum('monto');
            $totales['saldos'][$i] = $totales['ingresos'][$i] - $totales['egresos'][$i];
        }

        return view('transaccion.index')->with([
            'transacciones' => $transacciones,
            'totales' => $totales,
            'anio' => $anio
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transaccion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['user_id'] = Auth::user()->id;

        if($this->validate($request, [
            'fecha' => 'required',
            'descripcion' => 'required|min:5|max:255',
            'tipo' => 'required',
            'monto' => 'numeric|required',
            'user_id' => 'required'
        ])){
            $transaccion = new Transaccion([
                'user_id' => $request['user_id'],
                'fecha' => $request['fecha'],
                'descripcion' => $request['descripcion'],
                'tipo' => $request['tipo'],
                'monto' => $request['monto'],
            ]);

            $transaccion->save();

            Flash::success("Transacción registrada de forma exitosa");
        }else{
            Flash::danger("Ha ocurrido un error al registrar la transacción");
        }
        return redirect('transaccion');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Transaccion  $transaccion
     * @return \Illuminate\Http\Response
     */
    public function show(Transaccion $transaccion)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaccion  $transaccion
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaccion = Transaccion::findOrFail($id);

        return view('transaccion.edit')->with([
            'transaccion' => $transaccion
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaccion  $transaccion
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        if($this->validate($request, [
            'id' => 'required',
            'fecha' => 'required',
            'descripcion' => 'required|min:5|max:255',
            'tipo' => 'required',
            'monto' => 'numeric|required',
            'user_id' => 'required'
        ])){
            # crear arreglo con los campos que solo se permitirán cambiar
            $cambios = array(
                'fecha' => $request['fecha'],
                'descripcion' => $request['descripcion'],
                'tipo' => $request['tipo'],
                'monto' => $request['monto'],
            );

            # Se busca el registro a actualizar
            $transaccion = Transaccion::where('id', (int)$id)
                    ->where('user_id', (int)Auth::user()->id)
                    ->first();

            # se actualizan los datos
            $transaccion->update($cambios);

            Flash::success("Transacción actualizada de forma exitosa");
        }else{
            Flash::danger("Ha ocurrido un error al actualizar la transacción");
        }

        return redirect('transaccion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaccion  $transaccion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaccion $transaccion)
    {
        $transaccion->delete();
        Flash::success("Transacción eliminada.");
        return $this->index();
    }
}
