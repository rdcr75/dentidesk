<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaccion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','fecha', 'descripcion', 'tipo','monto'
    ];

    // Relacion de uno a muchos inversa (muchos a uno)
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    # Query Scope
    public function scopeFecha($query, $fecha)
    {
        if($fecha)
            return $query->where('fecha', 'LIKE', "$fecha%");
    }

    public function scopeDescripcion($query, $descripcion)
    {
        if($descripcion)
            return $query->where('descripcion', 'LIKE', "%$descripcion%");
    }
}
