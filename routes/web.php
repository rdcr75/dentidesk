<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('inicio');
});

Route::get('/informacion', function () {
    return view('informacion');
});

# Rutas de transacciones
Route::resource('transaccion','TransaccionController');
// Route::get('transaccion','TransaccionController@index')->name('transaccion');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
